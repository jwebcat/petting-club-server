# Digitopia

This is an example Strongloop app Scaffolding.

The details of this repository are discussed on [digitopia](http://blog.digitopia.com/)

####Installation

To run the app:

1. Clone this repo

3. `cd` into the cloned repo

4. run `npm install`

6. run `grunt`

7. run `./localdev.sh node .`
